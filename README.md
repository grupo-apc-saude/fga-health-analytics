# FGA Health Analytics

Dashboard de acompanhamento dos índices de covid-19, em âmbito global analisando os países de forma geral, e em nacional, analisando os estados do Brasil.

---
# Resumo

O projeto consiste em um levantamento de dados sobre casos, óbitos e pacientes recuperados da recente pandemia mundial. 
Essa análise será feita em âmbito global analisando os países e em âmbito nacional, avaliando as regiões e estados do Brasil.  
Além disso, com os dados levantados traçaremos um perfil do índice de mortalidade do vírus, índice de recuperação e qual a faixa etária com maiores riscos em relação ao Covid-19.

---
# Fontes de Dados
Nosso dataset pode ser encontrado aqui:

- [Dataset municípios e estados brasileiros](https://brasil.io/dataset/covid19/caso_full/)
- [Dataset dos países](https://ourworldindata.org/coronavirus-source-data)

---
# Esboço

**Produto**: Dashboard de acompanhamento da pandemia do Coronavírus.

**Cliente**: Disseminadores de informações.

**Público-Alvo**: Pessoas em geral interessadas em informações sobre a pandemia.

**Projetistas**: (Em aberto).

**Declaração sobre o problema**: A informação da pandemia do Coronavírus, embora existente e bem detalhada, chega ao público de forma fragmentada e pouco esclarecedora.

**Declaração sobre o projeto**: Levantamento de dados sobre o número de casos, óbitos e pacientes recuperados do Coronavírus.

---
# Protótipo

Precisamos inserir o protótipo do dashboard.
